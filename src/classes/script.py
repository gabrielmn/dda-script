import win32gui, win32con, win32api, time, threading
from .window import Window

class Script:

    heroes = [
        "None",
        "Monk",
        "Summoner"
    ]

    hero = None
    key_control = 0x11
    key_g = 0x47
    key_f = 0x46

    online = False
    multiplayer = False
    delay_solo = 0.5
    delay_multiplayer = 1.5

    def __init__(self):
        
        window_class = "UnrealWindow"
        window_name = None

        self.window_handler = win32gui.FindWindow(window_class, window_name)

        self.window = Window(heroes=self.heroes, set_hero=self.set_hero, toggle_online=self.toggle_online, toggle_multiplayer=self.toggle_multiplayer)


    def set_hero(self, *args):
        self.hero = args[0]

    def toggle_online(self):
        self.online = not self.online
    
    def toggle_multiplayer(self):
        self.multiplayer = not self.multiplayer

    def run(self):

        thread = None

        while self.window.is_open:

            if self.online:
                if thread == None or not thread.is_alive():     
                    thread = threading.Thread(target=self.thread, name="test", daemon=True)
                    thread.start()

            self.window.root.update()

        self.window.root.mainloop()
    
    def send_ready(self):

        if not self.multiplayer:
            win32api.SendMessage(self.window_handler, win32con.WM_KEYDOWN, self.key_g , 0)
            time.sleep(self.delay_solo)
            win32api.SendMessage(self.window_handler, win32con.WM_KEYUP, self.key_g, 0)
        else:
            win32api.SendMessage(self.window_handler, win32con.WM_KEYDOWN, self.key_control, 0)
            win32api.SendMessage(self.window_handler, win32con.WM_KEYDOWN, self.key_g , 0)
            time.sleep(self.delay_multiplayer)
            win32api.SendMessage(self.window_handler, win32con.WM_KEYUP, self.key_control, 0)
            win32api.SendMessage(self.window_handler, win32con.WM_KEYUP, self.key_g, 0)

    def send_buff(self):
        
        if self.hero == None or self.hero == self.heroes[0]:
            return

        win32api.SendMessage(self.window_handler, win32con.WM_KEYDOWN, self.key_f , 0)
        time.sleep(0.0625) 
        win32api.SendMessage(self.window_handler, win32con.WM_KEYUP, self.key_f, 0)
        
        delay = self.delay_multiplayer if self.multiplayer else self.delay_solo

        if self.hero == self.heroes[1]:
            print(self.hero)
            time.sleep(16 - delay)
        elif self.hero == self.heroes[2]:
            print(self.hero)
            time.sleep(7 - delay)

    def thread(self):
        self.send_ready()
        self.send_buff()