import tkinter

class Window:

    def __init__(self, heroes, set_hero, toggle_online, toggle_multiplayer):
        self.root = tkinter.Tk()
        self.root.title("DDA Script")
        self.root.configure(padx=16, pady=16)
        self.root.protocol("WM_DELETE_WINDOW", func=self.on_close_window)
        self.root.geometry("384x512")

        self.is_open = True

        self.label_status = tkinter.Label(self.root, text="Status: stopped")
        self.label_status.pack(anchor="w", side=tkinter.TOP, pady=(0, 24))
        self.label_status.configure(font=("Arial", 24))

        self.label_mode = tkinter.Label(self.root, text="Mode: solo")
        self.label_mode.pack(anchor="w", side=tkinter.TOP, pady=(0, 24))
        self.label_mode.configure(font=("Arial", 24))

        self.label_menu_hero = tkinter.Label(self.root, text="Select the hero that will boost")
        self.label_menu_hero.pack(anchor="n", side=tkinter.TOP, pady=(24, 8))
        self.label_menu_hero.configure(font=("Arial", 14))

        self.option_menu_hero_value = tkinter.StringVar(None, "hero")
        self.option_menu_hero = tkinter.OptionMenu(self.root, self.option_menu_hero_value, *heroes, command=set_hero)
        self.option_menu_hero.pack(anchor="n", side=tkinter.TOP)
        self.option_menu_hero.configure( font=("Arial", 14))

        self.button_toggle_multiplayer = tkinter.Button(self.root, text="Multiplayer", command=lambda :self.on_click_multiplayer(toggle_multiplayer))
        self.button_toggle_multiplayer.pack(anchor="sw", side=tkinter.LEFT, ipadx=16, ipady=4, pady=(96,0))
        self.button_toggle_multiplayer.configure( font=("Arial", 14))

        self.button_toggle_online = tkinter.Button(self.root, text="Start", command=lambda: self.on_click_online(toggle_online))
        self.button_toggle_online.pack(anchor="se", side=tkinter.RIGHT, ipadx=16, ipady=4, pady=(96,0))
        self.button_toggle_online.configure( font=("Arial", 14))
    
    def on_close_window(self):
        self.is_open = False
        self.root.destroy()
    
    def on_click_online(self, callback):

        if self.button_toggle_online["text"] == "Stop":
            self.button_toggle_online.configure(text="Start")
            self.label_status.configure(text="Status: stopped")
        else:
            self.button_toggle_online.configure(text="Stop")
            self.label_status.configure(text="Status: running")
        
        callback()
    
    def on_click_multiplayer(self, callback):
        
        if self.button_toggle_multiplayer["text"]  == "Multiplayer":
            self.button_toggle_multiplayer.configure(text="Solo")
            self.label_mode.configure(text="Mode: multiplayer")
        else:
            self.button_toggle_multiplayer.configure(text="Multiplayer")
            self.label_mode.configure(text="Mode: solo")

        callback()