# DDA Script

***
- [DDA Script](#dda-script)
  - [Features](#features)
  - [Roadmap TODO](#roadmap-todo)
  - [Usage TODO](#usage-todo)
  - [Support TODO](#support-todo)
  - [Developing](#developing)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
      - [Python](#python)
      - [Windows API for Python](#windows-api-for-python)
      - [Tkinter](#tkinter)
      - [PyInstaller](#pyinstaller)
    - [Run](#run)
    - [Build](#build)
  - [Contributing TODO](#contributing-todo)
  - [Authors and acknowledgment TODO](#authors-and-acknowledgment-todo)
  - [License TODO](#license-todo)

***
## Features
This script can do the following:
- Auto ready solo
- Auto ready multiplayer
- Auto buff:
  - Defense Boost
  - Flash Heal

## Roadmap TODO
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Usage TODO
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support TODO
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Developing

### Prerequisites

This project uses the following:
- [Python](https://www.python.org/)
- [Windows API for python](https://pypi.org/project/pywin32/)
- [Tkinter](https://docs.python.org/3/library/tkinter.html)
- [PyInstaller](https://pyinstaller.org/en/stable/)

### Installation

#### Python

Go to [Python website](https://www.python.org/downloads/) and download the version 3.10+ and follow the installation wizard.

Now you will have Python and pip installed on your computer.
#### Windows API for Python

To install this simply run the command bellow on the cmd or powershell.
```
pip install pywin32
```

#### Tkinter

To install this simply run the command bellow on the cmd or powershell.
```
pip install tk
```
#### PyInstaller

To install this simply run the command bellow on the cmd or powershell.
```
pip install pyinstaller
```

### Run

To run without building just use the following command.
```
py.exe src/main.py
```

### Build

To build use the following command.
```
pyinstaller.exe -F .\src\main.py --noconsole
```
## Contributing TODO
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment TODO
Show your appreciation to those who have contributed to the project.

## License TODO
For open source projects, say how it is licensed.